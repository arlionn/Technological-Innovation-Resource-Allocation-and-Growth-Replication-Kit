# Replication Code and Data for Kogan, L., Papanikolaou, D., Seru, A. and Stoffman, N., QJE 2017 

> 原仓库地址：<https://github.com/KPSS2017/Technological-Innovation-Resource-Allocation-and-Growth-Replication-Kit>    

> &#x1F34E; Note: 数据文件需要到原仓库逐个下载。批量下载整个仓库的方式无法正确下载数据文件。(Update: `2021/7/19 12:38`)

&emsp; 

This package provides the replication code for the main results in **Kogan, L., Papanikolaou, D., Seru, A. and Stoffman, N., 2017. Technological innovation, resource allocation, and growth. Quarterly Journal of Economics, 132(2), pp. 665-712.** The paper is available at https://academic.oup.com/qje/article/132/2/665/3076284.

The folder ./Code contains all programs, while the folder ./Data includes all needed input data files. 

## Code 

The folder ./Code includes code files of two types of programs: Stata and Matlab. 

### Code files using Stata:

#### Prerequisite:

In order for the code to run correctly, you need the following packages installed in Stata: 

- **winsorizeJ**: Winsorize variables using breakpoints that vary by year. (A winsorizeJ.ado file is provided in the folder ./Code)

- **cluster2**: 2D clustered standard errors. (A cluster2.ado file is provided in the folder ./Code)

- **ivreg2**: IV regression with more features than STATA's ivreg. (Install the package using command "ssc install ivreg2")

- **reghdfe**: Regression with multiple levels of fixed effects. (Install the package using command "ssc install reghdfe")

- **estout**: Output Stata tables in tex format.  (Install the package using command "ssc install estout")

#### File description:

- **FilterReturnsCreateFirmMeasures.do**: Creates the firm-level innovation measure using patent and stock return data

- **PatentValueCites.do**: Descriptive statistics for the patent-level measure, including robustness for alternative distributions (Table 1 in the paper and Table A.6 in the Online Appendix); relates the estimated patent values to forward citations (Table 2 in the paper and Table A.7 in the Online Appendix)

- **CreateFirmSample.do**: Creates the data for the firm-level regressions

- **FirmSummaryStats.do**: Descriptive statistics for the firm-level measure (Table 3 in the paper)
 
- **FirmProfitsRegressionSM.do**: Firm Profits and innovation (Panel a of Table 4 in the paper) 

  **FirmOutputRegressionSM.do**: Firm Output and innovation (Panel b of Table 4 in the paper) 
  
  **FirmReallocationRegressionSM.do**: Firm Capital/Labor and innovation (Panels c and d of Table 4 in the paper ) 
  
  **FirmTFPRegressionSM.do**: Firm TFP and innovation (Panel e of Table 4 in the paper) 	

- **FirmProfitsRegressionCW.do**: Firm Profits and innovation using citation-weighted patents (Panel a of Table 5 in the paper)

  **FirmOutputRegressionCW.do**: Firm Output and innovation using citation-weighted patents (Panel b of Table 5 in the paper)
  
  **FirmReallocationRegressionCW.do**: Firm Capital/Labor and innovation using citation-weighted patents (Panels c and d of Table 5 in the paper) 
  
  **FirmTFPRegressionCW.do**: Firm TFP and innovation using citation-weighted patents (Panel e of Table 5 in the paper) 

- **FirmProfitsRegressionSMCW.do**: Firm Profits and innovation using both our measure and citation-weighted patents (Panel a of Table 6 in the paper) 

  **FirmOutputRegressionSMCW.do**: Firm Output and innovation using both our measure and citation-weighted patents (Panel b of Table 6 in the paper)
  
  **FirmReallocationRegressionSMCW.do**: Firm Capital/Labor and innovation using both our measure and citation-weighted patents (Panels c and d of Table 6 in the paper)
  
  **FirmTFPRegressionSMCW.do**: Firm TFP and innovation using both our measure and citation-weighted patents (Panel e of Table 6 in the paper)

- **PatentValueScatterPlot.do**:  Plots the cross-sectional relation between forward patent citations and patent market value (Figure 2 in the paper)

- **TimeSeriesPlots.do**: Produces and plots the aggregate measures of innovation (Figure 4 in the paper) 
	
- **AggregateOutput.do**: Runs the aggregate OLS regressions and VARs that relate our two innovation indices to output and TFP (The regression results for Figure 5  in the paper and Figure A.3 in the Online Appendix)



### Code files using Matlab:

#### Prerequisite:

In order for the code to run correctly, you need to install the following program as well: 

- **matlab2tikz**: Converts MATLAB®/Octave figures to TikZ/pgfplots figures for smooth integration into LaTeX. (Please go to https://github.com/matlab2tikz/matlab2tikz for detailed installation information)

#### File description:

- **plot_OLS_responses.m**: Take the output of AggregateOutput.do and creates Figure 5 in the paper

- **csvimport.m**: Define the function called in plot_OLS_responses.m which helps import the .csv files

- **jbfill.m**: Define the function called in plot_OLS_responses.m which helps plot the confidence intervals


## Additional Notes:

- There is a minor discrepancy between the value of average acceptance probabilty used in the codes and reported on page 677 (published version of the paper). This minor difference is due to a typo in the published version.

- There are some minor discrepancies between the output that the code generates and the results reported in Table 5 (published version of the paper). These minor differences are due to typos in the published version.

- There are copyright restrictions and file size limitations in posting daily data from CRSP. Please contact the authors below to obtain the code and data needed to replicate Figures 1 and 3 in the paper that use this data.


## Contact

Please contact Dimitris Papanikolaou (d-papanikolaou@kellogg.northwestern.edu) or Amit Seru (aseru@stanford.edu) for any questions regarding the codes or data.

**Please see the paper for more information on the codes and data. If you use these codes files or data, please CITE this paper as the source.**




&emsp;
 
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> Stata连享会 &ensp; [主页](https://www.lianxh.cn/news/46917f1076104.html) || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) || [Bilibili 站](https://space.bilibili.com/546535876)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-草料主页-一码平川600.png)

&emsp;

## 1. 连享会课程

> **免费公开课：**    
- [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[课程主页](https://gitee.com/arlionn/PanelData)，[B 站版](https://www.bilibili.com/video/BV1oU4y187qY)
- [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [B 站版](https://space.bilibili.com/546535876/channel/detail?cid=160748)   
- [Stata 小白的取经之路](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) - 龙志能，时长：2 小时，[课程主页](https://gitee.com/arlionn/StataBin) 
- 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等) 

&emsp;

> ### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom02.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom03.png)](https://www.lianxh.cn/news/46917f1076104.html)

> ### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

## 2. 资源分享

### 视频公开课

- [连享会码云：100多个精选计量项目](https://www.lianxh.cn/news/944a69d75cec9.html) |  [新浪视频](https://weibo.com/tv/show/1034:4479228373303338)
- [五分钟 Markdown / Markdown 幻灯片](https://gitee.com/arlionn/md) | [新浪视频](https://weibo.com/tv/show/1034:4484204327796746)
- [连老师给你的-听课建议](https://www.lianxh.cn/news/69706e871c9ad.html)
- [直击面板数据模型](http://lianxh-pc.duanshu.com/course/detail/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[B 站版](https://www.bilibili.com/video/BV1oU4y187qY)
- [Stata 33 讲](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/arlionn/stata101)，[课件](https://gitee.com/arlionn/stata101)，[B 站版](https://space.bilibili.com/546535876/channel/detail?cid=160748)    
- [Stata小白的取经之路](https://gitee.com/arlionn/StataBin)，龙志能，上海财经大学，[去听课](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) 

### Stata

- [连享会推文](https://www.lianxh.cn) | [直播视频](http://lianxh.duanshu.com)
- **计量专题课程**: [Stata暑期班/寒假班](https://gitee.com/lianxh/text) | [专题课程](https://gitee.com/arlionn/Course)
- Stata专栏：[最新推文](https://www.lianxh.cn) | [知乎](https://www.zhihu.com/people/arlionn/) | [CSDN](https://blog.csdn.net/arlionn) | [Bilibili 站](https://space.bilibili.com/546535876)
- Books and Journal: [计量Books](https://quqi.gblhgk.com/s/880197/hmpmu2ylAcvHnXwY) | [SJ-PDF](https://quqi.gblhgk.com/s/880197/eipgoUi6Gd1FDZRu) | [Stata Journal-在线浏览](https://www.lianxh.cn/news/12ffe67d8d8fb.html)
- Stata Guys：[Ben Jann](http://www.soz.unibe.ch/about_us/personen/prof_dr_jann_ben/index_eng.html) 

### Data
- [CSMAR-国泰安](http://www.gtarsc.com/#/datacenter/singletable) | [Wind-万德](https://www.wind.com.cn/Default.html) | [Resset-锐思](http://www.resset.cn/databases)
- [常用数据库](https://www.lianxh.cn/news/0b65fd5165c2c.html) 
- [人文社科开放数据库](https://www.lianxh.cn/news/6f06c914acde8.html) 
- [徐现祥教授-IRE-官员交流、方言等](https://www.lianxh.cn/news/8c9f81a5f19ee.html)
- [知乎-Data](https://www.zhihu.com/question/20179699/answer/681756635)

### Papers - 学术论文复现
- [论文重现网站](https://www.lianxh.cn/news/e87e5976686d5.html)
- [Google学术](https://ac.scmor.com/) | [统一入口：虫部落学术搜索](http://scholar.chongbuluo.com/) | [微软学术](https://academic.microsoft.com/home)
- [iData - 期刊论文下载](https://www.cn-ki.net/)
- [ CNKI ](http://scholar.cnki.net/) | [百度学术](http://xueshu.baidu.com/) | [Google学术](https://scholar.glgoo.org/) | [Sci-hub ](http://www.sci-hub.cc/), [2](http://sci-hub.ac/), [3](http://sci-hub.bz/), [4](http://sci-hub.ac/)
- Stata论文重现:  [Harvard dataverse][harvd] | [JFE][jfe]  | [github][git1] | [Yahoo-github][yahoogit]
- 学者主页(提供了诸多论文的原始数据和 dofiles)：[Angrist][Ang1] || [Daron Acemoglu][acem]  || [Ross Levine][ross] || [Esther Duflo][Duflo] || [Imbens](https://scholar.harvard.edu/imbens/software)  ||  [Raj Chetty](http://www.rajchetty.com/)

[harvd]:https://dataverse.harvard.edu/dataverse
[jfe]:http://jfe.rochester.edu/data.htm
[Ang1]:http://economics.mit.edu/faculty/angrist/data1/data
[acem]:http://economics.mit.edu/faculty/acemoglu/data
[ross]:http://faculty.haas.berkeley.edu/ross_levine/papers.htm
[duflo]:http://economics.mit.edu/faculty/eduflo/papers
[git1]:https://github.com/search?utf8=%E2%9C%93&q=stata&type=

[yahoogit]:https://search.yahoo.com/search;_ylt=AwrBT8di2LBZqyEAuG9XNyoA;_ylc=X1MDMjc2NjY3OQRfcgMyBGZyA3lmcC10LTQ3MwRncHJpZAMEbl9yc2x0AzAEbl9zdWdnAzAEb3JpZ2luA3NlYXJjaC55YWhvby5jb20EcG9zAzAEcHFzdHIDBHBxc3RybAMwBHFzdHJsAzE0BHF1ZXJ5A3N0YXRhJTIwZ2l0aHViBHRfc3RtcAMxNTA0NzYxODcz?p=stata+github&fr2=sb-top&fr=yfp-t-473&fp=1

&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，500+ 推文，实证分析不再抓狂；[Bilibili 站](https://space.bilibili.com/546535876) 有视频大餐。

&emsp; 

> &#x26F3;  **`lianxh` 命令发布了：**    
> 随时搜索连享会推文、Stata 资源，安装命令如下：  
> &emsp; `. ssc install lianxh`  
> 使用详情参见帮助文件 (有惊喜)：   
> &emsp; `. help lianxh`


&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

&emsp;



